﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using SharpUniFi.Messages;

namespace SharpUniFi
{
    class Program
    {
        static void Main(string[] args)
        {
            var username = GetUsername();
            var password = ReadPassword();
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            RunAsync("https://dashboard.vertica.dk:8443/", username, password).Wait();
        }

        static async Task RunAsync(string baseAddress, string username, string password)
        {
            var emptyPostForm = new FormUrlEncodedContent(new Dictionary<string, string> {{"json", "{}"}});

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler {
                AllowAutoRedirect = true,
                UseCookies = true,
                CookieContainer = cookieContainer
            })
            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(baseAddress);
                var response = await client.PostAsync("login", new FormUrlEncodedContent(new Dictionary<string, string>
                {
                    {"username",username},
                    {"password",password},
                    {"login", "login"}
                }));
                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Login success!");
                }
                else
                {
                    Console.WriteLine("Login FAILED!");
                }

                response = await client.PostAsync("api/s/default/stat/sta", emptyPostForm);
                response.EnsureSuccessStatusCode();
                var users = await response.Content.ReadAsAsync<Users>();

                foreach (var ap in users.Data.GroupBy(x => x.ApMac))
                {
                    Console.WriteLine();
                    Console.WriteLine("AP: {0}", ap.Key);
                    foreach (var user in ap)
                    {
                        Console.WriteLine("{0}: {1} rssi={2}", user.Mac, user.Hostname, user.Rssi);
                    }
                }
            }
        }

        private static string GetUsername()
        {
            Console.Write("Enter username: ");
            return (Console.ReadLine() ?? "").Trim();
        }

        private static string ReadPassword()
        {
            string pass = "";
            Console.Write("Enter your password: ");
            ConsoleKeyInfo key;

            do
            {
                key = Console.ReadKey(true);

                // Backspace Should Not Work
                if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
                {
                    pass += key.KeyChar;
                    Console.Write("*");
                }
                else
                {
                    if (key.Key == ConsoleKey.Backspace && pass.Length > 0)
                    {
                        pass = pass.Substring(0, (pass.Length - 1));
                        Console.Write("\b \b");
                    }
                }
            }
            // Stops Receving Keys Once Enter is Pressed
            while (key.Key != ConsoleKey.Enter);
            Console.WriteLine();
            return pass;
        }

    }



    class UniFi
    {

    }


}
